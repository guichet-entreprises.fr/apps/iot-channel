@page DAL
Cube : Dossier d'Architecture Logiciel
============================================

\tableofcontents

\section desc Description des exigences

\subsection exigMet Exigences métiers

Le projet « IoT channel » (« cube ») permet d’interagir avec des objets connectés Sigfox, chez les partenaires, sur lesquels seront envoyés les informations relatives aux dossiers à traiter.

\subsection cntrPerf Contrainte de performance

Temps d’affichage d’une page (utilisateurs internes) : &lt; 10s

\subsection cntrDispo Contrainte de de disponibilité

Interruptions de services prévisibles : installation, mise à jour applicative.

Aucune contrainte de disponibilité n'est définie.

\subsection cntrVolum Contrainte de volumétrie

-   Base de données « iot_cube » :
    -   Aucune donnée n’est stockée pour le moment

\subsection archiLogique Schéma de l'architecture logique
![Schéma de l'architecture logique](@ref schema_architecture_logique.jpg)

\section archi Architecture logicielle

\subsection decompo Décomposition en sous-systèmes applicatifs

>   Description des nœuds applicatifs et des services que chacun rend.

| **type de noeud applicatif** | **nom de noeud applicatif** | **description**                                                              |
|------------------------------|-----------------------------|------------------------------------------------------------------------------|
| apache_vhost                 | VH_IOT_CUBE                 | virtual host Apache qui sert APP_IOT_CUBE                                    |
| tomcat_appli                 | APP_IOT_CUBE                | application web qui contient des IHM privées et quelques webservices publics |
| sgbd_postgres                | DB_IOT_CUBE                 | base de données de APP_IOT_CUBE                                              |

>   Explication des types de noeuds applicatifs : 

| **type de noeud applicatif** | **description**                                                                                                                                           |
|------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------|
| apache_vhost                 | un virtual host qui est le point d'entrée (public ou local) vers l'application. Ce noeud contient uniquement de la configuration.                         |
| apache_appli                 | une application contenue dans un serveur Apache, une application Wordpress ou maison comme le support. De base ce genre de noeud inclut son virtual host. |
| tomcat_appli                 | une application contenue dans un serveur Tomcat.                                                                                                          |
| sgbd_mysql                   | un schéma applicatif contenu dans un SGBD MySql.                                                                                                          |
| sgbd_postgres                | un schéma applicatif contenu dans un SGBD Postgres.                                                                                                       |

\subsection descMacro Description macroscopique des flux internes

>   Liste des flux internes au SI ou au VLAN, à choisir

| **Origine**    | **Destination** | **Protocol** | **Port** |
|----------------|-----------------|--------------|----------|
| VH_IOT_CUBE    | APP_IOT_CUBE    | AJP13        | 8019     |
| APP_IOT_CUBE   | DB_ IOT_CUBE    | JDBC         | 5432     |
| SIGFOX         | VH_IOT_CUBE     | HTTP       6 | 80       |

\subsection cineAlim Cinématique d'alimentation

>   Sans objet.

\subsection descMacroFlux Description macroscopique des flux externes

Sigfox fait appel aux webservices exposés sous /api/public.

\subsection exploit Eléments d'exploitation

Aucune remarque.
