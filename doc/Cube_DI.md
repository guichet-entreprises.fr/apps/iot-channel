@page DI
Cube : Dossier d'installation
===================================

\tableofcontents

\section compoBulle Composants de la bulle Cube

\subsection webclient iot-webclient

IHM privée et Web Services publics pour Sigfox

-   nom de projet : iot-channel

-   URL : https://cube.guichet-partenaires.fr

-   bulle : CUBE

| **GroupID**              | **Nom du fichier**                   | **Description**                                                                                                                     |
|--------------------------|--------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------|
| fr.ge.common.channel.iot | iot-webclient-*x.y.z*.war            | Module applicatif                                                                                                                   |
| fr.ge.common.channel.iot | iot-webclient-*x.y.z*-config.zip     | Fichier(s) de propriétés                                                                                                            |
| fr.ge.common.channel.iot | iot-data-services-*x.y.z*-bdd.tar.gz | Librairie du module applicatif contenant les scripts d'initialisation de la base de données utilisable en l'état avec **FlywayDB**. |

* *x.y.z* représente la version livrée.

\subsection confSocle Configuration du socle logiciel

Doivent être provisionnés sur les serveurs les composants suivant :

| **Noeud applicatif** | **Description**              | **Composants techniques requis** |
|----------------------|------------------------------|----------------------------------|
| VH_IOT_CUBE        | Frontal du module applicatif | Apache                           |
| APP_IOT_CUBE       | Module applicatif            | Tomcat 8.0                       |
| DB_IOT_CUBE        | Base de données              | PostgreSQL 9.4                   |

\subsection instBase Installation de la base de données

Les scripts SQL versionnés se trouvent dans le livrable
**iot-data-services-x.y.z-bdd.tar.gz** . Ils sont à exécuter avec l'outil
FlywayDB.

\subsection instVirtualHost Installation du virtual host

| **Nom**  | **Description**           | **&lt; PROD**                         | **PROD**                    |
|----------|---------------------------|-------------------------------------|-----------------------------|
| IoT Cube | URL de l'instance interne | cube.${env}.guichet-partenaires.fr | cube.guichet-partenaires.fr |

**Il y aura deux virtual host : un pour les api public et un autre pour l’IHM**.

Pour l'exemple, dans le répertoire **&nbsp;/etc/apache2/sites-enabled**, ajouter un virtualhost (ie **XX-cube.${env}.guichet-partenaires.fr.conf**) redirigeant vers **la partie public** des api de l'application :

~~~~~~~~~~~~~{.xml}
	#********************************************
	# Vhost template in module puppetlabs-apache
	# Managed by Puppet
	#********************************************
	
	<VirtualHost *:8080>
		ServerName cube.${env}.guichet-partenaires.fr
		
		## Vhost docroot
		DocumentRoot "/var/www/ge"
		
		## Directories, there should at least be a declaration for /var/www/ge
		<Location "/status">
			Require valid-user
			AuthType Basic
			AuthName "status"
			AuthUserFile /usr/passwd/status/htpasswd
		</Location>
		
		<Directory "/var/www/ge">
			Options None
			AllowOverride None
			Require all denied
		</Directory>
		
		## Logging
		ErrorLog "/var/log/apache2/cube.${env}.guichet-partenaires.fr_error.log"
		ServerSignature Off
		CustomLog "/var/log/apache2/cube.${env}.guichet-partenaires.fr_access.log"
		combined
		
		JkMount /public Cube
		
	</VirtualHost>
~~~~~~~~~~~~~
Pour l'exemple, dans le répertoire <strong>/etc/apache2/sites-enabled</strong>, ajouter un virtualhost (ie <strong>XX-cube.${env}.guichet-partenaires.loc.conf</strong>) redirigeant vers <strong>l’IHM</strong> de l'application :

~~~~~~~~~~~~~{.xml}
	# ************************************
	# Vhost template in module puppetlabs-apache
	# Managed by Puppet
	# ************************************
	<VirtualHost *:8080>
		ServerName cube.${env}.guichet-partenaires.loc
		
		## Vhost docroot
		DocumentRoot "/var/www/ge"
		
		## Directories, there should at least be a declaration for /var/www/ge
		
		<Location "/status">
			Require valid-user
			AuthType Basic
			AuthName "status"
			AuthUserFile /usr/passwd/status/htpasswd
		</Location>
		<Directory "/var/www/ge">
			Options None
			AllowOverride None
			Require all denied
		</Directory>
		
		## Logging
		ErrorLog "/var/log/apache2/cube.${env}.guichet-partenaires.loc_error.log"
		ServerSignature Off
		CustomLog "/var/log/apache2/cube.${env}.guichet-partenaires.loc_access.log"
		combined
		
		JkMount /private feedback

	</VirtualHost>
~~~~~~~~~~~~~

Il est nécessaire d'activer le module **mod_jk** d'Apache gérant la
communication et la répartition de charge entre Apache et les serveurs Tomcat.

Ajouter dans le répertoire **/etc/apache2** le fichier **workers.properties**,
celui-ci contenant la configuration du module Apache :

worker.list=jkstatus,ws-feedback

worker.maintain=240
worker.jkstatus.type=status
worker.server1.port=8009
worker.server1.host=cube.${env}.guichet-partenaires.loc
worker.server1.type=ajp13
worker.server1.lbfactor=1
worker.server1.connection_pool_size=250
worker.server1.connection_pool_timeout=600
worker.server1.socket_timeout=900
worker.server1.socket_keepalive=True

worker.markov.balance_workers=server1
worker.markov.type=lb

\subsection instWar Installation de l'archive Web

L'installation se fait sous **/var/lib/tomcat8** .

L'archive Web de l'application doit être déposée sous **/srv/appli/ge/common/cube**.

La configuration Tomcat nécessaire pour le démarrage de l'application se trouve sous **/var/lib/tomcat8/conf/**.

- Configuration du serveur Catalina : **Catalina/cube.${env}.guichet-partenaires.fr/ROOT.xml**   
  Le context doit pointer sur le war décrit plus haut.  
  On doit également décrire les propriétés de connexion de l'application à la
  base de données **iot_cube**.

**Catalina/.../ROOT.xml**

~~~~~~~~~~~~~{.xml}
	<?xml version="1.0" encoding="UTF-8"?>
	<Context reloadable="true" docBase="/srv/appli/ge/common/cube/iot-webclient.war">
		<Resources className="org.apache.catalina.webresources.StandardRoot">
		<PreResources className="org.apache.catalina.webresources.DirResourceSet" base="/srv/conf/ge/common/cube" webAppMount="/WEB-INF/classes" />
		</Resources>
		<Resource name="jdbc/iot_cube" auth="Container"
		type="javax.sql.DataSource" driverClassName="org.postgresql.Driver"
		url="jdbc:postgresql://data.${env}.guichet-partenaires.loc:5432/cube?charSet=UNICODE"
		defaultAutoCommit="true" initialSize="1"
		validationQuery="SELECT 1"
		username="ge_feedback_app" password="ge_feedback_pwd"
		maxTotal="20" maxIdle="10" maxWaitMillis="10000"
		removeAbandonedOnBorrow="true" removeAbandonedTimeout="60" logAbandoned="true"/>
	</Context>
~~~~~~~~~~~~~

Ajout de l'hôte virtuel sur la configuration du serveur :
**/var/lib/tomcat8/conf/server.xml** .

Ajout de la ligne :

~~~~~~~~~~~~~{.xml}
	<Host name="cube.${env}.guichet-partenaires.loc"
		appBase="webapps_cube "
		unpackWARs="true"
		autoDeploy="true" />
~~~~~~~~~~~~~

\subsection confApp Configuration de l'application

La configuration externalisée de l'application doit être déposée sous <strong>/srv/conf/ge/common/cube</strong>.

\subsubsection confProperties fichier application.properties

| **Paramètre**      | **Description**                  | **DEV**       | **QUAL**      | **INT**     | **RECJ**      | **RECR**      | **PPROD**      | **PROD** |
|--------------------|----------------------------------|---------------|---------------|-------------|---------------|---------------|----------------|----------|
| environment        | Libellé de l'environnement       | Développement | Qualification | Intégration | Recette Jaune | Recette Rouge | Pré Production | <vide> |
| jndi.db.datasource | Nom JNDI de la source de données | jdbc/cube     |               |             |               |               |                |          |
| cube.public.url    | URI API publique de Cube         | exemple :     |               |             |               |               |                |          |

\subsubsection fichierLog4j fichier log4j2.xml

| **Paramètre**       | **Description**                                             | **DEV**                                | **QUAL** | **INT** | **RECJ** | **RECR** | **PPROD** | **PROD** |
|---------------------|-------------------------------------------------------------|----------------------------------------|----------|---------|----------|----------|-----------|----------|
| log.level           | Niveau de log par défaut                                    | Debug                                  | debug    | info    | info     | info     | notice    | notice   |
| log.level_app      | Niveau de log propre au module applicatif                   | debug                                  | debug    | info    | info     | info     | notice    | notice   |
| log.level_fwk      | Niveau de log des librairies externes                       | info                                   | info     | info    | notice   | notice   | warn      | warn     |
| log.file.technical  | Chemin du fichier contenant les logs techniques             | /src/log/ge/common/cube/tech.log       |          |         |          |          |           |          |
| log.file.functional | Chemin du fichier contenant les logs fonctionnels           | /src/log/ge/common/cube/func.log       |          |         |          |          |           |          |
| log.file.thirdParty | Chemin du fichier contenant les logs des libraires externes | /src/log/ge/common/cube/thirdParty.log |          |         |          |          |           |          |
| log.max_size       | Taille maximale des fichiers de logs journalisés            | 10 MB                                  |          |         |          |          |           |          |
| log.max_index      | Index maximum des fichiers de logs journalisés              | 20                                     |          |         |          |          |           |          |
