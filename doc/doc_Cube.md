Documentation Cube                         {#mainpage}
============

\tableofcontents

\section intro_sec Introduction

Bienvenu dans la documentation Cube du Guichet Entreprises !

![cube](@ref cube.jpeg)

\section install_sec Installation

\subsection dossierInstallation Dossier d'installation : 
  \ref DI "Dossier d'installation"
\subsection dossierArchiLogique Dossier d'architecture logique : 
  \ref DAL "Dossier d'architecture logique"

\section utilisation Utilisation

![utilisation du cube](@ref cube_manuel.jpg)