/**
 * 
 */
package fr.ge.common.channel.iot.core;

/**
 * * \mainpage Documentation Cube * \section intro_sec Introduction * * Bienvenu
 * dans la documentation Cube du Guichet Entreprises ! * * \section install_sec
 * Installation * * \subsection step1 Dossier d'installation : * \ref DI
 * "Dossier d'installation" * \subsection step2 Dossier d'architecture logique :
 * * \ref DAL "Dossier d'architecture logique" *
 */
public class App {

    /**
     * @param args
     */
    public static void main(String[] args) {
        System.out.println("Hello World!");
    }

}
