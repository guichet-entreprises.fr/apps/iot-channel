/**
 *
 */
package fr.ge.common.channel.iot.bean;

import java.util.Date;
import java.util.Optional;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * Representing received messages from Sigfox.
 *
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class PendingMessageBean {

    /** Id. */
    private Long id;

    /** The device identifier. **/
    private String deviceId;

    /** The message creation date. */
    private Date created;

    /** The message sending date. */
    private Date sent;

    /** The message content. */
    private byte[] message;

    /**
     * Accesseur sur l'attribut {@link #deviceId}.
     *
     * @return String deviceId
     */
    public String getDeviceId() {
        return this.deviceId;
    }

    /**
     * Mutateur sur l'attribut {@link #deviceId}.
     *
     * @param deviceId
     *            la nouvelle valeur de l'attribut deviceId
     */
    public void setDeviceId(final String deviceId) {
        this.deviceId = deviceId;
    }

    /**
     * Accesseur sur l'attribut {@link #created}.
     *
     * @return Date created
     */
    public Date getCreated() {
        return this.created;
    }

    /**
     * Mutateur sur l'attribut {@link #created}.
     *
     * @param created
     *            la nouvelle valeur de l'attribut created
     */
    public void setCreated(final Date created) {
        this.created = created;
    }

    /**
     * Gets the sent.
     *
     * @return the sent
     */
    public Date getSent() {
        return this.sent;
    }

    /**
     * Sets the sent.
     *
     * @param sent
     *            the new sent
     */
    public void setSent(final Date sent) {
        this.sent = sent;
    }

    /**
     * Accesseur sur l'attribut {@link #message}.
     *
     * @return byte[] message
     */
    public byte[] getMessage() {
        return (byte[]) Optional.ofNullable(this.message).map(byte[]::clone).orElse(null);

    }

    /**
     * Mutateur sur l'attribut {@link #message}.
     *
     * @param message
     *            la nouvelle valeur de l'attribut message
     */
    public void setMessage(final byte[] message) {
        this.message = (byte[]) Optional.ofNullable(message).map(byte[]::clone).orElse(null);
    }

    /**
     * Accesseur sur l'attribut {@link #id}.
     *
     * @return Long id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Mutateur sur l'attribut {@link #id}.
     *
     * @param id
     *            la nouvelle valeur de l'attribut id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
    }

}
