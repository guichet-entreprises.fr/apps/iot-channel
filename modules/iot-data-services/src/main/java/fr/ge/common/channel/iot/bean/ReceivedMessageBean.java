/**
 *
 */
package fr.ge.common.channel.iot.bean;

import java.util.Date;
import java.util.Optional;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * Representing received messages from Sigfox.
 *
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
public class ReceivedMessageBean {

    /** Id. */
    private Long id;

    /** The device identifier. **/
    private String deviceId;

    /** The message creation date. */
    private Date created;

    /** The message reception date. */
    private Date received;

    /** The message sequence number. **/
    private Long seqNumber;

    /** The message content. */
    private byte[] message;

    /**
     * Accesseur sur l'attribut {@link #deviceId}.
     *
     * @return String deviceId
     */
    public String getDeviceId() {
        return this.deviceId;
    }

    /**
     * Mutateur sur l'attribut {@link #deviceId}.
     *
     * @param deviceId
     *            la nouvelle valeur de l'attribut deviceId
     */
    public void setDeviceId(final String deviceId) {
        this.deviceId = deviceId;
    }

    /**
     * Accesseur sur l'attribut {@link #created}.
     *
     * @return Date created
     */
    public Date getCreated() {
        return this.created;
    }

    /**
     * Mutateur sur l'attribut {@link #created}.
     *
     * @param created
     *            la nouvelle valeur de l'attribut created
     */
    public void setCreated(final Date created) {
        this.created = created;
    }

    /**
     * Accesseur sur l'attribut {@link #received}.
     *
     * @return Date received
     */
    public Date getReceived() {
        return this.received;
    }

    /**
     * Mutateur sur l'attribut {@link #received}.
     *
     * @param received
     *            la nouvelle valeur de l'attribut received
     */
    public void setReceived(final Date received) {
        this.received = received;
    }

    /**
     * Accesseur sur l'attribut {@link #seqNumber}.
     *
     * @return sequence number
     */
    public Long getSeqNumber() {
        return this.seqNumber;
    }

    /**
     * Mutateur sur l'attribut {@link #seqNumber}.
     *
     * @param seqNumber
     *            la nouvelle valeur de l'attribut seqNumber
     */
    public void setSeqNumber(final Long seqNumber) {
        this.seqNumber = seqNumber;
    }

    /**
     * Accesseur sur l'attribut {@link #message}.
     *
     * @return byte[] message
     */
    public byte[] getMessage() {
        return (byte[]) Optional.ofNullable(this.message).map(byte[]::clone).orElse(null);

    }

    /**
     * Mutateur sur l'attribut {@link #message}.
     *
     * @param message
     *            la nouvelle valeur de l'attribut message
     */
    public void setMessage(final byte[] message) {
        this.message = (byte[]) Optional.ofNullable(message).map(byte[]::clone).orElse(null);
    }

    /**
     * Accesseur sur l'attribut {@link #id}.
     *
     * @return Long id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Mutateur sur l'attribut {@link #id}.
     *
     * @param id
     *            la nouvelle valeur de l'attribut id
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.JSON_STYLE);
    }

}
