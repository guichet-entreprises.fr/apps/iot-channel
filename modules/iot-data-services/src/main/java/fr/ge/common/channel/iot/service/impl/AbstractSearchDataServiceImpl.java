/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.channel.iot.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.ibatis.session.RowBounds;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;

import fr.ge.common.channel.iot.service.ISearchDataService;
import fr.ge.common.channel.iot.service.mapper.SearchMapper;
import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchResult;

/**
 * Data search service implementation.
 *
 * @author Christian Cougourdan
 *
 * @param <T>
 *            the data type to search
 */
public abstract class AbstractSearchDataServiceImpl<T> implements ISearchDataService {

    /** Dozer. */
    @Autowired
    private DozerBeanMapper dozer;

    /**
     * {@inheritDoc}
     */
    @Override
    public <R> SearchResult<R> search(final SearchQuery searchQuery, final Class<R> expectedClass) {
        final Map<String, SearchQueryFilter> filters = new HashMap<String, SearchQueryFilter>();
        if (null != searchQuery.getFilters()) {
            searchQuery.getFilters().forEach(filter -> filters.put(filter.getColumn(), filter));
        }

        final RowBounds rowBounds = new RowBounds((int) searchQuery.getStartIndex(), (int) searchQuery.getMaxResults());
        final List<T> entities = this.getMapper().findAll(filters, searchQuery.getOrders(), rowBounds);

        final SearchResult<R> searchResult = new SearchResult<>(searchQuery.getStartIndex(), searchQuery.getMaxResults());
        if (null != entities) {
            searchResult.setContent(entities.stream().map(elm -> this.dozer.map(elm, expectedClass)).collect(Collectors.toList()));
        }

        searchResult.setTotalResults(this.getMapper().count(filters));

        return searchResult;
    }

    /**
     * Gets the mapper.
     * 
     * @return the mapper
     */
    protected abstract SearchMapper<T> getMapper();

}
