/**
 *
 */
package fr.ge.common.channel.iot.service.impl;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.ge.common.channel.iot.bean.PendingMessageBean;
import fr.ge.common.channel.iot.service.IPendingMessageDataService;
import fr.ge.common.channel.iot.service.mapper.PendingMessagesMapper;
import fr.ge.common.channel.iot.service.mapper.SearchMapper;

/**
 * Received message service implementation.
 *
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
@Service("pendingMessageDataService")
public class PendingMessageDataServiceImpl extends AbstractSearchDataServiceImpl<PendingMessageBean> implements IPendingMessageDataService {

    @Autowired
    private DozerBeanMapper dozer;

    /** The received messages mapper. **/
    @Autowired
    private PendingMessagesMapper pendingMessagesMapper;

    @Override
    protected SearchMapper<PendingMessageBean> getMapper() {
        return this.pendingMessagesMapper;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Long add(final PendingMessageBean pendingMessageBean) {
        return this.pendingMessagesMapper.add(pendingMessageBean);
    }

    @Override
    public <R> R findById(final long id, final Class<R> expectedClass) {
        final PendingMessageBean bean = this.pendingMessagesMapper.findById(id);
        return this.dozer.map(bean, expectedClass);
    }

}
