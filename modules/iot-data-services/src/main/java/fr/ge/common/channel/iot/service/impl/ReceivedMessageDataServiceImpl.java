/**
 *
 */
package fr.ge.common.channel.iot.service.impl;

import java.time.Instant;
import java.util.Calendar;
import java.util.Date;

import org.dozer.DozerBeanMapper;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import fr.ge.common.channel.iot.bean.ReceivedMessageBean;
import fr.ge.common.channel.iot.service.IReceivedMessageDataService;
import fr.ge.common.channel.iot.service.mapper.ReceivedMessagesMapper;
import fr.ge.common.channel.iot.service.mapper.SearchMapper;
import fr.ge.common.utils.exception.TechnicalException;

/**
 * Received message service implementation.
 *
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
@Service("receivedMessageDataService")
public class ReceivedMessageDataServiceImpl extends AbstractSearchDataServiceImpl<ReceivedMessageBean> implements IReceivedMessageDataService {

    @Autowired
    private DozerBeanMapper dozer;

    /** The received messages mapper. **/
    @Autowired
    private ReceivedMessagesMapper receivedMessagesMapper;

    @Override
    protected SearchMapper<ReceivedMessageBean> getMapper() {
        return this.receivedMessagesMapper;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String add(final String data) {
        String deviceId = null;
        String received = null;
        String seqNumber = null;
        final JSONParser parser = new JSONParser();

        JSONObject json;
        try {
            json = (JSONObject) parser.parse(data);
            if (json.containsKey("deviceId")) {
                deviceId = json.get("deviceId").toString();
            }
            if (json.containsKey("time")) {
                received = json.get("time").toString();
            }
            if (json.containsKey("seqNumber")) {
                seqNumber = json.get("seqNumber").toString();
            }
        } catch (final ParseException e) {
            throw new TechnicalException("The data message could not be parsed", e);
        }

        if (StringUtils.isEmpty(deviceId) || StringUtils.isEmpty(received) || StringUtils.isEmpty(data)) {
            throw new TechnicalException("The required fields are not present in the message content");
        }

        final Date now = Calendar.getInstance().getTime();

        final ReceivedMessageBean receivedMessage = new ReceivedMessageBean();
        receivedMessage.setDeviceId(deviceId);
        receivedMessage.setMessage(data.getBytes());
        receivedMessage.setCreated(now);
        if (!StringUtils.isEmpty(seqNumber)) {
            receivedMessage.setSeqNumber(Long.parseLong(seqNumber));
        }
        if (!StringUtils.isEmpty(received)) {
            final Integer receivedInt = Integer.parseInt(received);
            final Instant instant = Instant.ofEpochSecond(receivedInt);
            receivedMessage.setReceived(Date.from(instant));
        }
        this.receivedMessagesMapper.add(receivedMessage);

        return deviceId;
    }

    @Override
    public <R> R findById(final long id, final Class<R> expectedClass) {
        final ReceivedMessageBean bean = this.receivedMessagesMapper.findById(id);
        return this.dozer.map(bean, expectedClass);
    }
}
