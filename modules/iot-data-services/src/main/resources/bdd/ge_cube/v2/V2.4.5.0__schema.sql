CREATE TABLE received_messages (
    id				BIGINT	PRIMARY KEY,
    device_id		CHAR(6) NOT NULL,
	created			TIMESTAMP NOT NULL,
	received		TIMESTAMP NOT NULL,
    seq_number		BIGINT,
    message			BYTEA
);

ALTER TABLE received_messages ADD CONSTRAINT pk_received_messages UNIQUE (id);

CREATE SEQUENCE sq_received_messages START 1 MINVALUE 1 CACHE 1;