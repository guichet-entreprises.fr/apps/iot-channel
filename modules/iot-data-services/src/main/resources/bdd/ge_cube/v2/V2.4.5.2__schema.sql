DROP TABLE IF EXISTS pending_messages;
DROP SEQUENCE IF EXISTS sq_pending_messages;

CREATE TABLE pending_messages (
    id              BIGINT	PRIMARY KEY,
    device_id       CHAR(6) NOT NULL,
    created         TIMESTAMP NOT NULL,
    sent            TIMESTAMP,
    message         BYTEA
);

ALTER TABLE pending_messages ADD CONSTRAINT pk_pending_messages UNIQUE (id);

CREATE SEQUENCE sq_pending_messages START 1 MINVALUE 1 CACHE 1;
