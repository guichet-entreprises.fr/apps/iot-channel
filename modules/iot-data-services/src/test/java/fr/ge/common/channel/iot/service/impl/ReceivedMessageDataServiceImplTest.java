/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.channel.iot.service.impl;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.common.channel.iot.bean.ReceivedMessageBean;
import fr.ge.common.channel.iot.service.IReceivedMessageDataService;
import fr.ge.common.channel.iot.service.data.AbstractDbTest;
import fr.ge.common.channel.iot.service.mapper.ReceivedMessagesMapper;
import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchResult;
import fr.ge.common.utils.exception.TechnicalException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/test-context.xml", "classpath:spring/test-persistence-context.xml" })
public class ReceivedMessageDataServiceImplTest extends AbstractDbTest {

    /** Authority data service. */
    @Autowired
    IReceivedMessageDataService receivedMessageDataService;

    /** Received messages mapper. */
    @Autowired
    ReceivedMessagesMapper receivedMessagesMapper;

    /**
     * Setup.
     *
     * @throws Exception
     *             exception
     */
    @Before
    public void setUp() throws Exception {
        Mockito.reset(this.receivedMessagesMapper);
    }

    /**
     * Formats a String to be at the JSON format.
     *
     * @param str
     *            the input string
     * @return the string with the good format
     */
    private String json(final String str) {
        return str.replaceAll("'", "\"");
    }

    /**
     * Add a correct received message.
     */
    @Test
    public void testAddReceivedMessage() {
        final String data = this.json("{ 'deviceId': 'ABCABC', 'time': 1508159, 'data': '11016100000000003C000000', 'RSSI': -128, 'seqNumber': 1, 'baseStationId': '2ABE' }");
        final String deviceId = this.receivedMessageDataService.add(data);
        assertThat(deviceId, equalTo("ABCABC"));
        verify(this.receivedMessagesMapper).add(any());
    }

    /**
     * Add a received message with wrong elements.
     */
    @Test(expected = TechnicalException.class)
    public void testAddReceivedMessageWithException() {
        final String data = this.json("{ 'email': 'guy.saint-hilaire@channel.com' }");
        this.receivedMessageDataService.add(data);
    }

    /**
     * Add a received message with no JSON message.
     */
    @Test(expected = TechnicalException.class)
    public void testAddReceivedMessageWithParseException() {
        final String data = this.json("guy.saint-hilaire@channel.com");
        this.receivedMessageDataService.add(data);
    }

    /**
     * Gets a received message with an ID.
     */
    @Test
    public void testGet() {
        // prepare
        final Date now = Calendar.getInstance().getTime();
        final String message = this.json("{ 'deviceId': 'ABCABC', 'time': 1508159, 'data': '11016100000000003C000000', 'RSSI': -128, 'seqNumber': 1, 'baseStationId': '2ABE' }");
        final ReceivedMessageBean receivedMessage = new ReceivedMessageBean();
        receivedMessage.setId(42L);
        receivedMessage.setCreated(now);
        receivedMessage.setReceived(now);
        receivedMessage.setDeviceId("ABCABC");
        receivedMessage.setMessage(message.getBytes());
        receivedMessage.setSeqNumber(1L);
        when(this.receivedMessagesMapper.findById(eq(42L))).thenReturn(receivedMessage);

        // call
        final ReceivedMessageBean result = this.receivedMessageDataService.findById(42L);

        // verify
        verify(this.receivedMessagesMapper).findById(eq(42L));
        assertThat(result,
                allOf( //
                        hasProperty("id", equalTo(42L)), //
                        hasProperty("deviceId", equalTo("ABCABC")), //
                        hasProperty("seqNumber", equalTo(1L)), //
                        hasProperty("message", equalTo(message.getBytes())) //
                ) //
        );
    }

    /**
     * Search received messages.
     */
    @Test
    public void testSearch() {
        final Date now = Calendar.getInstance().getTime();
        final String message = this.json("{ 'deviceId': 'ABCABC', 'time': 1508159, 'data': '11016100000000003C000000', 'RSSI': -128, 'seqNumber': 1, 'baseStationId': '2ABE' }");
        final ReceivedMessageBean receivedMessage = new ReceivedMessageBean();
        receivedMessage.setId(43L);
        receivedMessage.setCreated(now);
        receivedMessage.setReceived(now);
        receivedMessage.setDeviceId("ABCABC");
        receivedMessage.setMessage(message.getBytes());
        receivedMessage.setSeqNumber(1L);
        final List<ReceivedMessageBean> dataSearchResult = Arrays.asList(receivedMessage, receivedMessage, receivedMessage);
        when(this.receivedMessagesMapper.findAll(any(Map.class), eq(null), any(RowBounds.class))).thenReturn(dataSearchResult);

        final SearchQuery searchQuery = new SearchQuery(0, 5);
        final SearchResult<ReceivedMessageBean> result = this.receivedMessageDataService.search(searchQuery, ReceivedMessageBean.class);

        verify(this.receivedMessagesMapper).findAll(any(Map.class), eq(null), any(RowBounds.class));
    }

    /**
     * Search received messages with filters.
     */
    @Test
    public void testSearchWithFilters() {
        final Date now = Calendar.getInstance().getTime();
        final String message = this.json("{ 'deviceId': 'ABCABC', 'time': 1508159, 'data': '11016100000000003C000000', 'RSSI': -128, 'seqNumber': 1, 'baseStationId': '2ABE' }");
        final ReceivedMessageBean receivedMessage = new ReceivedMessageBean();
        receivedMessage.setId(43L);
        receivedMessage.setCreated(now);
        receivedMessage.setReceived(now);
        receivedMessage.setDeviceId("ABCABC");
        receivedMessage.setMessage(message.getBytes());
        receivedMessage.setSeqNumber(1L);
        final List<ReceivedMessageBean> dataSearchResult = Arrays.asList(receivedMessage, receivedMessage, receivedMessage);
        when(this.receivedMessagesMapper.findAll(any(Map.class), eq(null), any(RowBounds.class))).thenReturn(dataSearchResult);
        when(this.receivedMessagesMapper.count(any(Map.class))).thenReturn(3L);

        final SearchQuery searchQuery = new SearchQuery(0, 5).addFilter(new SearchQueryFilter("device_id", ":", "ABCABC"));
        final SearchResult<ReceivedMessageBean> result = this.receivedMessageDataService.search(searchQuery, ReceivedMessageBean.class);

        verify(this.receivedMessagesMapper).findAll(any(Map.class), eq(null), any(RowBounds.class));
        verify(this.receivedMessagesMapper).count(any(Map.class));
    }
}
