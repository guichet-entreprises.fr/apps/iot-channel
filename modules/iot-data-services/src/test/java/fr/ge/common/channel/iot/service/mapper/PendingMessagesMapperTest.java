/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.channel.iot.service.mapper;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.hasProperty;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.RowBounds;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.common.channel.iot.bean.PendingMessageBean;
import fr.ge.common.channel.iot.service.data.AbstractDbTest;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/test-context.xml" })
public class PendingMessagesMapperTest extends AbstractDbTest {

    /** The pending message mapper. */
    @Autowired
    private PendingMessagesMapper pendingMessagesMapper;

    /**
     * Setup.
     *
     * @throws Exception
     *             exception
     */
    @Before
    public void setUp() throws Exception {
        this.initDb(this.resourceName("dataset.xml"));
    }

    /**
     * Add a pending message.
     */
    @Test
    public void testAdd() {
        final PendingMessageBean pendingMessage = new PendingMessageBean();
        pendingMessage.setDeviceId("1");
        final Calendar calendar = Calendar.getInstance();
        pendingMessage.setCreated(calendar.getTime());
        pendingMessage.setSent(calendar.getTime());
        final String message = "{ 'deviceId': 'ABCABC', 'time': 1508159, 'data': '11016100000000003C000000', 'RSSI': -128, 'seqNumber': 1, 'baseStationId': '2ABE' }".replaceAll("'", "\"");
        pendingMessage.setMessage(message.getBytes());
        this.pendingMessagesMapper.add(pendingMessage);
    }

    /**
     * Gets a pending message.
     */
    @Test
    public void testGet() {
        assertThat(this.pendingMessagesMapper.findById(1001L),
                allOf( //
                        hasProperty("id", equalTo(1001L)), //
                        hasProperty("deviceId", equalTo("CUBE_1")) //
                ) //
        );
    }

    /**
     * Gets a pending message.
     */
    @Test
    public void testFindAll() {
        final Map<String, SearchQueryFilter> filters = new HashMap<String, SearchQueryFilter>();
        SearchQueryFilter sqf = new SearchQueryFilter();
        sqf.setColumn("deviceId");
        sqf.setOperator(":");
        sqf.setValue("CUBE_1");
        filters.put("deviceId", sqf);

        sqf = new SearchQueryFilter();
        sqf.setColumn("created");
        sqf.setOperator("<");
        sqf.setValue(new SimpleDateFormat("YYYY-MM-dd").format(new Date()));
        filters.put("created", sqf);
        final List<SearchQueryOrder> orders = new ArrayList<>();
        final RowBounds rowBounds = new RowBounds(0, 10);
        final List<PendingMessageBean> result = this.pendingMessagesMapper.findAll(filters, orders, rowBounds);
        assertThat(result,
                hasItems( //
                        allOf( //
                                hasProperty("id", equalTo(1001L)), //
                                hasProperty("deviceId", equalTo("CUBE_1")) //
                        ) //
                )//
        );
    }
}
