/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.channel.iot.client.controller.message.pending;

import java.nio.charset.StandardCharsets;
import java.util.Calendar;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import fr.ge.common.channel.iot.client.bean.PendingMessageClientBean;
import fr.ge.common.channel.iot.client.exception.DataNotFoundException;
import fr.ge.common.channel.iot.ws.v1.bean.ResponsePendingMessageBean;
import fr.ge.common.channel.iot.ws.v1.service.IPendingMessageRestService;

/**
 * @author Christian Cougourdan
 */
@Controller
@RequestMapping("/message/pending/edit")
public class PendingMessageEditController {

    @Autowired
    private IPendingMessageRestService pendingMessageRestService;

    @RequestMapping(method = RequestMethod.GET)
    public String page(final Model model, @RequestParam(value = "id", required = false) final Long id) throws DataNotFoundException {
        ResponsePendingMessageBean src = null;

        if (null == id) {
            src = new ResponsePendingMessageBean();
        } else {
            src = Optional.ofNullable(this.pendingMessageRestService.findById(id)).orElseThrow(() -> new DataNotFoundException(id));
        }

        final PendingMessageClientBean bean = new PendingMessageClientBean() //
                .setId(src.getId()) //
                .setDeviceId(src.getDeviceId()) //
                .setData(new String(src.getMessage(), StandardCharsets.UTF_8));

        model.addAttribute("bean", bean);

        return "message/pending/edit/main";
    }

    @RequestMapping(method = RequestMethod.POST)
    public String save(@ModelAttribute final PendingMessageClientBean bean) {
        final ResponsePendingMessageBean dst = new ResponsePendingMessageBean() //
                .setId(bean.getId()) //
                .setDeviceId(bean.getDeviceId()) //
                .setCreated(Calendar.getInstance().getTime()).setMessage(bean.getData().getBytes(StandardCharsets.UTF_8));

        this.pendingMessageRestService.create(dst);

        return "redirect:/message/pending/search";
    }

    @ExceptionHandler(DataNotFoundException.class)
    public ModelAndView dataNotFoundHandler(final HttpServletResponse response, final DataNotFoundException exception) {
        response.setStatus(HttpServletResponse.SC_NOT_FOUND);

        final ModelAndView modelAndView = new ModelAndView("message/pending/error/notfound");
        modelAndView.addObject("error", exception);

        return modelAndView;
    }
}
