/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.channel.iot.client.controller.message.pending;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.nio.charset.StandardCharsets;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import fr.ge.common.channel.iot.ws.v1.bean.ResponsePendingMessageBean;
import fr.ge.common.channel.iot.ws.v1.service.IPendingMessageRestService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath:spring/test-context.xml" })
@WebAppConfiguration
public class PendingMessageEditControllerTest {

    /** mvc. */
    private MockMvc mvc;

    /** web app context. */
    @Autowired
    private WebApplicationContext webAppContext;

    /** The service. */
    @Autowired
    private IPendingMessageRestService service;

    /**
     * Set up.
     *
     * @throws Exception
     *             exception
     */
    @Before
    public void setUp() throws Exception {
        this.mvc = MockMvcBuilders.webAppContextSetup(this.webAppContext).build();
        reset(this.service);
    }

    @Test
    public void testEdit() throws Exception {
        final ResponsePendingMessageBean bean = new ResponsePendingMessageBean() //
                .setId(1L) //
                .setDeviceId("ABCABC") //
                .setMessage("11016100000000003C000000".getBytes()) //
                .setCreated(new Date());

        when(this.service.findById(any())).thenReturn(bean);
        this.mvc.perform(get("/message/pending/edit").param("id", "1")) //
                .andExpect(status().isOk()) //
                .andExpect(view().name("message/pending/edit/main"));

        verify(this.service).findById(eq(1L));
    }

    @Test
    public void testEditNotFound() throws Exception {
        when(this.service.findById(any())).thenReturn(null);
        this.mvc.perform(get("/message/pending/edit").param("id", "1")) //
                .andExpect(status().isNotFound()) //
                .andExpect(view().name("message/pending/error/notfound"));

        verify(this.service).findById(eq(1L));
    }

    @Test
    public void testEditNew() throws Exception {
        this.mvc.perform(get("/message/pending/edit")) //
                .andExpect(status().isOk()) //
                .andExpect(view().name("message/pending/edit/main"));

        verify(this.service, never()).findById(any());
    }

    @Test
    public void testCreate() throws Exception {
        final ResponsePendingMessageBean bean = new ResponsePendingMessageBean() //
                .setDeviceId("ABCABC") //
                .setMessage("11016100000000003C000000".getBytes());

        this.mvc.perform(post("/message/pending/edit").param("deviceId", bean.getDeviceId()).param("data", new String(bean.getMessage(), StandardCharsets.UTF_8))) //
                .andExpect(status().is3xxRedirection()) //
                .andExpect(redirectedUrl("/message/pending/search"));

        final ArgumentCaptor<ResponsePendingMessageBean> captor = ArgumentCaptor.forClass(ResponsePendingMessageBean.class);
        verify(this.service).create(captor.capture());

        assertThat(captor.getValue(), //
                allOf( //
                        hasProperty("id", nullValue()), //
                        hasProperty("deviceId", equalTo(bean.getDeviceId())), //
                        hasProperty("message", equalTo(bean.getMessage())), //
                        hasProperty("sent", nullValue()) //
                ) //
        );
    }

}
