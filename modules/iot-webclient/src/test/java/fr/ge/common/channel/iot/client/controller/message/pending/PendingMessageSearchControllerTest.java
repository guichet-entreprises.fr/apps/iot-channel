/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.channel.iot.client.controller.message.pending;

import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import fr.ge.common.channel.iot.ws.v1.bean.ResponsePendingMessageBean;
import fr.ge.common.channel.iot.ws.v1.service.IPendingMessageRestService;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;
import fr.ge.common.utils.bean.search.SearchResult;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration({ "classpath:spring/test-context.xml" })
@WebAppConfiguration
public class PendingMessageSearchControllerTest {

    /** mvc. */
    private MockMvc mvc;

    /** web app context. */
    @Autowired
    private WebApplicationContext webAppContext;

    /** The service. */
    @Autowired
    private IPendingMessageRestService service;

    /**
     * Set up.
     *
     * @throws Exception
     *             exception
     */
    @Before
    public void setUp() throws Exception {
        this.mvc = MockMvcBuilders.webAppContextSetup(this.webAppContext).build();
        reset(this.service);
    }

    /**
     * Test search main.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testMain() throws Exception {
        this.mvc.perform(get("/message/pending/search")) //
                .andExpect(status().isOk()) //
                .andExpect(view().name("message/pending/search/main"));
    }

    /**
     * Test search with empty data.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testSearchEmptyData() throws Exception {
        final SearchResult<ResponsePendingMessageBean> searchResult = new SearchResult<>(0, 10);
        when(this.service.search(any(long.class), any(long.class), any(), any())).thenReturn(searchResult);

        this.mvc.perform( //
                get("/message/pending/search/data") //
                        .param("draw", "1") //
                        .param("start", "0") //
                        .param("length", "10") //
        ) //
                .andExpect(status().isOk());

        final ArgumentCaptor<List<SearchQueryFilter>> filtersCaptor = ArgumentCaptor.forClass(List.class);
        final ArgumentCaptor<List<SearchQueryOrder>> ordersCaptor = ArgumentCaptor.forClass(List.class);

        verify(this.service).search(eq(0L), eq(10L), filtersCaptor.capture(), ordersCaptor.capture());

        assertThat(filtersCaptor.getValue(), hasSize(0));
        assertThat(ordersCaptor.getValue(), empty());
    }

    /**
     * Test search with data.
     *
     * @throws Exception
     *             the exception
     */
    @Test
    public void testSearchData() throws Exception {
        final SearchResult<ResponsePendingMessageBean> searchResult = new SearchResult<>(0, 10);
        searchResult.setContent(Collections.singletonList(new ResponsePendingMessageBean()));

        when(this.service.search(any(long.class), any(long.class), any(), any())).thenReturn(searchResult);

        this.mvc.perform( //
                get("/message/pending/search/data") //
                        .param("draw", "1") //
                        .param("start", "0") //
                        .param("length", "10") //
        ) //
                .andExpect(status().isOk());

        final ArgumentCaptor<List<SearchQueryFilter>> filtersCaptor = ArgumentCaptor.forClass(List.class);
        final ArgumentCaptor<List<SearchQueryOrder>> ordersCaptor = ArgumentCaptor.forClass(List.class);

        verify(this.service).search(eq(0L), eq(10L), filtersCaptor.capture(), ordersCaptor.capture());

        assertThat(filtersCaptor.getValue(), empty());
        assertThat(ordersCaptor.getValue(), empty());
    }
}
