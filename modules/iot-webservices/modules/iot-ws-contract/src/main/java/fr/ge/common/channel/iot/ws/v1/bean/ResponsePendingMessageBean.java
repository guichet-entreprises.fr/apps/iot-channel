/**
 *
 */
package fr.ge.common.channel.iot.ws.v1.bean;

import java.util.Date;
import java.util.Optional;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Representing the received data into a bean.
 *
 * @author aolubi
 *
 */

@XmlRootElement(name = "pendingMessage", namespace = "http://v1.ws.cube.ge.fr")
@XmlAccessorType(XmlAccessType.FIELD)
public class ResponsePendingMessageBean {

    /** The id. */
    private Long id;

    /** The device identifier. **/
    private String deviceId;

    /** The data creation date. */
    private Date created;

    /** The data reception date. */
    private Date sent;

    /** The data content. */
    private byte[] message;

    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Sets the id.
     *
     * @param id
     *            the new id
     * @return the response pending message bean
     */
    public ResponsePendingMessageBean setId(final Long id) {
        this.id = id;
        return this;
    }

    /**
     * Gets the device id.
     *
     * @return the device id
     */
    public String getDeviceId() {
        return this.deviceId;
    }

    /**
     * Sets the device id.
     *
     * @param deviceId
     *            the new device id
     * @return the response pending message bean
     */
    public ResponsePendingMessageBean setDeviceId(final String deviceId) {
        this.deviceId = deviceId;
        return this;
    }

    /**
     * Gets the created.
     *
     * @return the created
     */
    public Date getCreated() {
        return this.created;
    }

    /**
     * Sets the created.
     *
     * @param created
     *            the new created
     * @return the response pending message bean
     */
    public ResponsePendingMessageBean setCreated(final Date created) {
        this.created = created;
        return this;
    }

    /**
     * Gets the sent.
     *
     * @return the sent
     */
    public Date getSent() {
        return this.sent;
    }

    /**
     * Sets the sent.
     *
     * @param sent
     *            the new sent
     * @return the response pending message bean
     */
    public ResponsePendingMessageBean setSent(final Date sent) {
        this.sent = sent;
        return this;
    }

    /**
     * Gets the message.
     *
     * @return the message
     */
    public byte[] getMessage() {
        return Optional.ofNullable(this.message).map(obj -> obj.clone()).orElse(new byte[] {});
    }

    /**
     * Sets the data.
     *
     * @param message
     *            the new message
     * @return the response pending message bean
     */
    public ResponsePendingMessageBean setMessage(final byte[] message) {
        this.message = Optional.ofNullable(message).map(obj -> obj.clone()).orElse(null);
        return this;
    }

}
