/**
 * 
 */
package fr.ge.common.channel.iot.ws.v1.bean;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Representing the received message into a bean.
 * 
 * @author aolubi
 *
 */

@XmlRootElement(name = "receivedMessage", namespace = "http://v1.ws.cube.ge.fr")
@XmlAccessorType(XmlAccessType.FIELD)
public class ResponseReceivedMessageBean {

    /** The id. */
    private Long id;

    /** The device identifier. **/
    private String deviceId;

    /** The message creation date. */
    private Date created;

    /** The message reception date. */
    private Date received;

    /** The message sequence number. **/
    private String seqNumber;

    /** The message content. */
    private byte[] message;

    /**
     * Constructor empty.
     */
    public ResponseReceivedMessageBean() {
        super();
    }

    /**
     * Accesseur sur l'attribut {@link #deviceId}.
     *
     * @return String deviceId
     */
    public String getDeviceId() {
        return deviceId;
    }

    /**
     * Mutateur sur l'attribut {@link #deviceId}.
     *
     * @param deviceId
     *            la nouvelle valeur de l'attribut deviceId
     */
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    /**
     * Accesseur sur l'attribut {@link #created}.
     *
     * @return Date created
     */
    public Date getCreated() {
        return created;
    }

    /**
     * Mutateur sur l'attribut {@link #created}.
     *
     * @param created
     *            la nouvelle valeur de l'attribut created
     */
    public void setCreated(Date created) {
        this.created = created;
    }

    /**
     * Accesseur sur l'attribut {@link #received}.
     *
     * @return Date received
     */
    public Date getReceived() {
        return received;
    }

    /**
     * Mutateur sur l'attribut {@link #received}.
     *
     * @param received
     *            la nouvelle valeur de l'attribut received
     */
    public void setReceived(Date received) {
        this.received = received;
    }

    /**
     * Accesseur sur l'attribut {@link #seqNumber}.
     *
     * @return String seqNumber
     */
    public String getSeqNumber() {
        return seqNumber;
    }

    /**
     * Mutateur sur l'attribut {@link #seqNumber}.
     *
     * @param seqNumber
     *            la nouvelle valeur de l'attribut seqNumber
     */
    public void setSeqNumber(String seqNumber) {
        this.seqNumber = seqNumber;
    }

    /**
     * Accesseur sur l'attribut {@link #id}.
     *
     * @return Long id
     */
    public Long getId() {
        return id;
    }

    /**
     * Mutateur sur l'attribut {@link #id}.
     *
     * @param id
     *            la nouvelle valeur de l'attribut id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Accesseur sur l'attribut {@link #message}.
     *
     * @return byte [] message
     */
    public byte[] getMessage() {
        return message;
    }

    /**
     * Mutateur sur l'attribut {@link #message}.
     *
     * @param message 
     *          la nouvelle valeur de l'attribut message
     */
    public void setMessage(byte[] message) {
        this.message = message;
    }
}
