/**
 *
 */
package fr.ge.common.channel.iot.ws.v1.service;

import java.util.List;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import fr.ge.common.channel.iot.ws.v1.bean.ResponsePendingMessageBean;
import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;
import fr.ge.common.utils.bean.search.SearchResult;

/**
 * @author bsadil
 *
 */
@Path("/private/v1/pending")
public interface IPendingMessageRestService {

    /**
     * Search for pending messages.
     *
     * @param startIndex
     *            start index
     * @param maxResults
     *            max results per page
     * @param filters
     *            filters as string
     * @param orders
     *            orders as string
     * @return search result
     */
    @GET
    @Produces({ MediaType.APPLICATION_JSON })
    SearchResult<ResponsePendingMessageBean> search(@QueryParam("startIndex") @DefaultValue(SearchQuery.DEFAULT_START_INDEX) long startIndex, //
            @QueryParam("maxResults") @DefaultValue(SearchQuery.DEFAULT_MAX_RESULTS) long maxResults, //
            @QueryParam("filters[]") List<SearchQueryFilter> filters, //
            @QueryParam("orders[]") List<SearchQueryOrder> orders //
    );

    /**
     * Create or update a pending message.
     *
     * @param bean
     *            message to persist
     * @return message URI
     */
    @POST
    @Produces({ MediaType.APPLICATION_JSON })
    Response create(ResponsePendingMessageBean bean);

    /**
     * Find and return a pending message identified by its technical key.
     *
     * @param id
     *            technical key
     * @return pending message
     */
    @GET
    @Path("/{id}")
    @Produces({ MediaType.APPLICATION_JSON })
    ResponsePendingMessageBean findById(@PathParam("id") Long id);

}
