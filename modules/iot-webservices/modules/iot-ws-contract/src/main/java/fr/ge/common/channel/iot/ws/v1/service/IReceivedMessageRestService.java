/**
 *
 */
package fr.ge.common.channel.iot.ws.v1.service;

import java.util.List;

import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import fr.ge.common.channel.iot.ws.v1.bean.ResponseReceivedMessageBean;
import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;
import fr.ge.common.utils.bean.search.SearchResult;

/**
 * @author bsadil
 *
 */
@Path("/private/v1/received")
public interface IReceivedMessageRestService {

    /**
     * Search for recevied messages.
     *
     * @param startIndex
     *            start index
     * @param maxResults
     *            max results per page
     * @param filters
     *            filters as string
     * @param orders
     *            orders as string
     * @return search result
     */
    @GET
    @Produces({ MediaType.APPLICATION_JSON })
    SearchResult<ResponseReceivedMessageBean> search(@QueryParam("startIndex") @DefaultValue(SearchQuery.DEFAULT_START_INDEX) long startIndex, //
            @QueryParam("maxResults") @DefaultValue(SearchQuery.DEFAULT_MAX_RESULTS) long maxResults, //
            @QueryParam("filters[]") List<SearchQueryFilter> filters, //
            @QueryParam("orders[]") List<SearchQueryOrder> orders //
    );

}
