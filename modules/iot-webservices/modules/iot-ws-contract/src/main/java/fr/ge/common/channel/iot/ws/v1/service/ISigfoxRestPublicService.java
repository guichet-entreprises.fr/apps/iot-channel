/**
 *
 */
package fr.ge.common.channel.iot.ws.v1.service;

import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

/**
 * SigFox REST public services.
 *
 * @author jpauchet
 */
@Path("/public/v1")
public interface ISigfoxRestPublicService {

    /**
     * Posts uplink messages from the IoT objects.
     *
     * @param accessToken
     *            the access token
     * @param data
     *            the data
     */
    @POST
    @Path("/data/uplink")
    @Consumes(MediaType.APPLICATION_JSON)
    void postUplinkMessages(@QueryParam("accessToken") String accessToken, String data);

    /**
     * Gets downlink messages for the IoT objects.
     *
     * @param accessToken
     *            the access token
     * @param data
     *            the data
     */
    @POST
    @Path("/data/bidir")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    Map<String, Map<String, String>> getDownlinkMessages(@QueryParam("accessToken") String accessToken, String data);

    /**
     * Gets geolocalization data from the IoT objects.
     *
     * @param accessToken
     *            the access token
     * @param data
     *            the data
     */
    @POST
    @Path("/service/geoloc")
    @Consumes(MediaType.APPLICATION_JSON)
    void getGeolocData(@QueryParam("accessToken") String accessToken, String data);

}
