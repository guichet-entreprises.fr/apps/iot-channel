/**
 *
 */
package fr.ge.common.channel.iot.ws.v1.service.impl;

import java.net.URI;
import java.util.List;

import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;

import fr.ge.common.channel.iot.bean.PendingMessageBean;
import fr.ge.common.channel.iot.service.IPendingMessageDataService;
import fr.ge.common.channel.iot.ws.v1.bean.ResponsePendingMessageBean;
import fr.ge.common.channel.iot.ws.v1.service.IPendingMessageRestService;
import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchQueryFilter;
import fr.ge.common.utils.bean.search.SearchQueryOrder;
import fr.ge.common.utils.bean.search.SearchResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

/**
 * @author bsadil
 *
 */
@Api("Pending Message REST Services")
@Path("/private/v1/pending")
public class PendingMessageRestServiceImpl implements IPendingMessageRestService {

    @Autowired
    private DozerBeanMapper dozer;

    @Autowired
    private IPendingMessageDataService dataService;

    /**
     * {@inheritDoc}
     */
    @Override
    @ApiOperation(value = "Search for pending messages", notes = "Multiple filters can be specified using pattern\n" //
            + "&lt;fieldName&gt;&lt;operator&gt;&lt;value&gt, where \"operator\" can be :\n" //
            + "- \":\" = equals\n" //
            + "- \"&gt;\" = greater than\n" //
            + "- \"&gt;=\" = greater than or equals\n" //
            + "- \"&lt;\" = less than\n" //
            + "- \"&lt;=\" = less than or equals\n")
    public SearchResult<ResponsePendingMessageBean> search(@ApiParam("page's first element index") final long startIndex, //
            @ApiParam("max element per page") final long maxResults, //
            @ApiParam("filters as \"&lt;fieldName&gt;&lt;operator&gt;&lt;value&gt;\"") final List<SearchQueryFilter> filters, //
            @ApiParam("orders as \"&lt;fieldName&gt;:&lt;asc|desc&gt;\"") final List<SearchQueryOrder> orders) {

        final SearchQuery searchQuery = new SearchQuery(startIndex, maxResults).setFilters(filters).setOrders(orders);
        return this.dataService.search(searchQuery, ResponsePendingMessageBean.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ApiOperation("Create or update a pending message")
    public Response create(@ApiParam("Pending message as JSON") final ResponsePendingMessageBean bean) {
        final Long id = this.dataService.add(this.dozer.map(bean, PendingMessageBean.class));
        return Response.created(URI.create("/private/v1/pending/" + id)).build();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @ApiOperation("Find and return a pending message identified by its technical key")
    public ResponsePendingMessageBean findById(@ApiParam("Technical key") final Long id) {
        return this.dataService.findById(id, ResponsePendingMessageBean.class);
    }

}
