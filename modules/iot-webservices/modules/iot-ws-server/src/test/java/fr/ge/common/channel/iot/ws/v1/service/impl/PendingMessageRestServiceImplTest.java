/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.channel.iot.ws.v1.service.impl;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Date;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.common.channel.iot.service.IPendingMessageDataService;
import fr.ge.common.channel.iot.ws.v1.bean.ResponsePendingMessageBean;
import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchResult;

/**
 * Testing sigfox Rest private services.
 *
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/data-context.xml", "classpath:spring/test-context.xml", "classpath:spring/iot-common-ws-context.xml" })
public class PendingMessageRestServiceImplTest extends AbstractRestTest {

    /** Received messages service. */
    @Autowired
    private IPendingMessageDataService dataService;

    /**
     * Setup.
     */
    @Before
    public void setup() {
        reset(this.dataService);
    }

    /**
     * Test search REST service.
     */
    @Test
    public void testSearch() {
        final Date now = new Date();

        final ResponsePendingMessageBean pendingMessage = new ResponsePendingMessageBean() //
                .setId(1L) //
                .setCreated(now) //
                .setDeviceId("ABCABC") //
                .setMessage("11016100000000003C000000".getBytes(StandardCharsets.UTF_8));

        final SearchResult<ResponsePendingMessageBean> dataSearchResult = new SearchResult<>(Long.parseLong(SearchQuery.DEFAULT_START_INDEX), Long.parseLong(SearchQuery.DEFAULT_MAX_RESULTS));
        dataSearchResult.setTotalResults(3L);
        dataSearchResult.setContent(Arrays.asList(pendingMessage, pendingMessage, pendingMessage));

        when(this.dataService.search(any(), eq(ResponsePendingMessageBean.class))).thenReturn(dataSearchResult);

        // call
        final Response responseSearch = this.client().accept(MediaType.APPLICATION_JSON).path("/private/v1/pending").query("startIndex", 0).query("maxResults", 5).get();

        assertThat(responseSearch, hasProperty("status", equalTo(200)));

        // verify
        verify(this.dataService).search(any(SearchQuery.class), eq(ResponsePendingMessageBean.class));
        assertThat(responseSearch.getStatus(), equalTo(Status.OK.getStatusCode()));
    }

    @Test
    public void testCreate() {
        when(this.dataService.add(any())).thenReturn(42L);

        final ResponsePendingMessageBean pendingMessage = new ResponsePendingMessageBean() //
                .setDeviceId("ABCABC") //
                .setMessage("11016100000000003C000000".getBytes(StandardCharsets.UTF_8));

        final Response response = this.client().path("/private/v1/pending").type(MediaType.APPLICATION_JSON).post(pendingMessage);

        assertThat(response, hasProperty("status", equalTo(201)));
        assertThat(response.getHeaderString("Location"), equalTo(ENDPOINT + "/private/v1/pending/42"));
    }

    @Test
    public void testFindById() {
        final ResponsePendingMessageBean bean = new ResponsePendingMessageBean() //
                .setId(42L) //
                .setDeviceId("ABCABC") //
                .setMessage("11016100000000003C000000".getBytes(StandardCharsets.UTF_8)) //
                .setCreated(new Date()) //
                .setSent(null);

        when(this.dataService.findById(eq(42L), eq(ResponsePendingMessageBean.class))).thenReturn(bean);

        final Response response = this.client().path("/private/v1/pending/{id}", 42).accept(MediaType.APPLICATION_JSON).get();

        assertThat(response, hasProperty("status", equalTo(200)));
    }

}
