/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.ge.common.channel.iot.ws.v1.service.impl;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasProperty;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Date;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.common.channel.iot.service.IReceivedMessageDataService;
import fr.ge.common.channel.iot.ws.v1.bean.ResponseReceivedMessageBean;
import fr.ge.common.utils.bean.search.SearchQuery;
import fr.ge.common.utils.bean.search.SearchResult;

/**
 * Testing sigfox Rest private services.
 *
 * @author $Author: aolubi $
 * @version $Revision: 0 $
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/data-context.xml", "classpath:spring/test-context.xml", "classpath:spring/iot-common-ws-context.xml" })
public class ReceivedMessageRestServiceImplTest extends AbstractRestTest {

    /** Received messages service. */
    @Autowired
    private IReceivedMessageDataService receivedMessageDataService;

    /**
     * Setup.
     */
    @Before
    public void setup() {
        reset(this.receivedMessageDataService);
    }

    /**
     * Formats a String to be at the JSON format.
     *
     * @param str
     *            the input string
     * @return the string with the good format
     */
    private String json(final String str) {
        return str.replaceAll("'", "\"");
    }

    /**
     * Test search REST service.
     */
    @Test
    public void testSearch() {
        final Date now = new Date();

        final ResponseReceivedMessageBean receivedMessage = new ResponseReceivedMessageBean();
        receivedMessage.setId(1L);
        receivedMessage.setCreated(now);
        receivedMessage.setSeqNumber("1");
        receivedMessage.setReceived(now);

        final String message = this.json("{ 'deviceId': 'ABCABC', 'time': 1508159, 'data': '11016100000000003C000000', 'RSSI': -128, 'seqNumber': 1, 'baseStationId': '2ABE' }");
        receivedMessage.setMessage(message.getBytes());

        final SearchResult<ResponseReceivedMessageBean> dataSearchResult = new SearchResult<>(Long.parseLong(SearchQuery.DEFAULT_START_INDEX), Long.parseLong(SearchQuery.DEFAULT_MAX_RESULTS));
        dataSearchResult.setTotalResults(3L);
        dataSearchResult.setContent(Arrays.asList(receivedMessage, receivedMessage, receivedMessage));

        when(this.receivedMessageDataService.search(any(), eq(ResponseReceivedMessageBean.class))).thenReturn(dataSearchResult);

        // call
        final Response responseSearch = this.client().type(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).path("/private/v1/received").query("startIndex", 0).query("maxResults", 5)
                .get();

        assertThat(responseSearch, hasProperty("status", equalTo(200)));

        // verify
        verify(this.receivedMessageDataService).search(any(SearchQuery.class), eq(ResponseReceivedMessageBean.class));
        assertThat(responseSearch.getStatus(), equalTo(Status.OK.getStatusCode()));
    }
}
