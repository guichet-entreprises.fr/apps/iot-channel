/*
 * Copyright SCN Guichet Entreprises, Capgemini and contributors (2016-2017)
 *
 * This software is a computer program whose purpose is to maintain and
 * administrate standalone forms.
 *
 * This software is governed by the CeCILL  license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package fr.ge.common.channel.iot.ws.v1.service.impl;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.ge.common.channel.iot.service.IReceivedMessageDataService;

/**
 * Tests {@link SigfoxRestPublicServiceImpl}.
 *
 * @author $Author: jpauchet $
 * @version $Revision: 0 $
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:spring/data-context.xml", "classpath:spring/test-context.xml", "classpath:spring/iot-common-ws-context.xml" })
public class SigfoxRestPublicServiceImplTest extends AbstractRestTest {

    /** Sigfox access token. */
    private static final String SIGFOX_ACCESS_TOKEN = "VDIRNUPHhQYKTpPLnlDzw6OPQZfwqUFV1vR5ypjlGxTKbpubGK3Gu0CqeSa8HmxZ";

    /** Received messages service. */
    @Autowired
    private IReceivedMessageDataService receivedMessageDataService;

    /**
     * Setup.
     */
    @Before
    public void setup() {
        Mockito.reset(this.receivedMessageDataService);
    }

    /**
     * Formats a String to be at the JSON format.
     *
     * @param str
     *            the input string
     * @return the string with the good format
     */
    private String json(final String str) {
        return str.replaceAll("'", "\"");
    }

    /**
     * Tests
     * {@link SigfoxRestPublicServiceImpl#getDownlinkMessages(String, String)}.
     */
    @Test
    public void testGetDownlinkMessages() throws Exception {
        // prepare
        final Map<String, Object> data = new HashMap<>();
        data.put("deviceId", "ABCABC");
        data.put("time", 1508159);
        data.put("data", "11016100000000003C000000");
        data.put("RSSI", -128);
        data.put("seqNumber", 1);
        data.put("baseStationId", "2ABE");

        // call
        final Response response = this.client().type(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).path("/public/v1/data/uplink") //
                .query("accessToken", SIGFOX_ACCESS_TOKEN).post(data);

        // verify
        verify(this.receivedMessageDataService).add(eq(this.json("{'RSSI':-128,'data':'11016100000000003C000000','seqNumber':1,'time':1508159,'deviceId':'ABCABC','baseStationId':'2ABE'}")));
        assertThat(response.getStatus(), equalTo(Status.NO_CONTENT.getStatusCode()));
    }

    /**
     * Tests
     * {@link SigfoxRestPublicServiceImpl#postUplinkMessages(String, String)}.
     */
    @Test
    public void testPostUplinkMessages() throws Exception {
        // prepare
        final Map<String, Object> data = new HashMap<>();
        data.put("deviceId", "ABCABC");
        data.put("time", 1508159);
        data.put("data", "11016100000000003C000000");
        data.put("RSSI", -128);
        data.put("seqNumber", 1);
        data.put("baseStationId", "2ABE");

        when(this.receivedMessageDataService.add(eq(this.json("{'RSSI':-128,'data':'11016100000000003C000000','seqNumber':1,'time':1508159,'deviceId':'ABCABC','baseStationId':'2ABE'}"))))
                .thenReturn("ABCABC");

        // call
        final Response response = this.client().type(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).path("/public/v1/data/bidir") //
                .query("accessToken", SIGFOX_ACCESS_TOKEN).post(data);

        // verify
        verify(this.receivedMessageDataService).add(eq(this.json("{'RSSI':-128,'data':'11016100000000003C000000','seqNumber':1,'time':1508159,'deviceId':'ABCABC','baseStationId':'2ABE'}")));
        assertThat(response.getStatus(), equalTo(Status.OK.getStatusCode()));
        assertThat(this.readAsString(response), equalTo(this.json("{'ABCABC':{'downlinkData':'11016106000000003C000000'}}")));
    }

    /**
     * Tests {@link SigfoxRestPublicServiceImpl#getGeolocData(String, String)}.
     */
    @Test
    public void testGetGeolocData() throws Exception {
        // prepare
        final Map<String, Object> data = new HashMap<>();
        data.put("deviceId", "ABCABC");
        data.put("time", 1508159);
        data.put("RSSI", -128);
        data.put("seqNumber", 1);
        data.put("baseStationId", "2ABE");
        data.put("lat", 48.866667);
        data.put("lng", 2.333333);
        data.put("precision", 1200);

        // call
        final Response response = this.client().type(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON).path("/public/v1/service/geoloc") //
                .query("accessToken", SIGFOX_ACCESS_TOKEN).post(data);

        // verify
        assertThat(response.getStatus(), equalTo(Status.NO_CONTENT.getStatusCode()));
    }

}
